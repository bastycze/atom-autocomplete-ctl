'use babel';

import BasicFunctionsProvider from './basic-functions-provider';
import BasicDatatypesProvider from './basic-datatypes-provider';


export default {
    getProvider() {
        // return a single provider, or an array of providers to use together
        return [BasicFunctionsProvider, BasicDatatypesProvider];
    }
};
