'use babel';

// load suggestion data from a file
import suggestions from '../data/functions';

class BasicFunctionsProvider {
	constructor() {
		// offer suggestions only when editing plain text or HTML files
		this.selector = '.source.ctlpp';

		// except when editing a comment within an HTML file
		this.disableForSelector = '.source.ctlpp .comment';

		// make these suggestions appear above default suggestions
		this.suggestionPriority = 2;
	}

	getSuggestions(options) {
		const { editor, bufferPosition } = options;

		// getting the prefix on our own instead of using the one Atom provides
		//let prefix = this.getPrefix(editor, bufferPosition);
		const { prefix } = options;

		if (prefix.length >= 3) {
			return this.findMatchingSuggestions(prefix);
		}
	}

	findMatchingSuggestions(prefix) {
		// filter json (list of suggestions) to those matching the prefix

		let matchingSuggestions = suggestions.filter((suggestion) => {
			let text = suggestion.displayText;
			return text.startsWith(prefix);
		});

		// bind a version of inflateSuggestion() that always passes in prefix
		// then run each matching suggestion through the bound inflateSuggestion()
		let inflateSuggestion = this.inflateSuggestion.bind(this, prefix);
		let inflatedSuggestions = matchingSuggestions.map(inflateSuggestion);

		return inflatedSuggestions;
	}

	// clones a suggestion object to a new object with some shared additions
	// cloning also fixes an issue where selecting a suggestion won't insert it
	inflateSuggestion(replacementPrefix, suggestion) {
		return {
			displayText: suggestion.displayText,
			snippet: suggestion.snippet,
			description: suggestion.description,
			descriptionMoreURL: suggestion.descriptionMoreURL,
			replacementPrefix: replacementPrefix, // ensures entire prefix is replaced
			iconHTML: '<i class="icon-comment"></i>',
			type: 'snippet',
			rightLabelHTML: '<span class="aab-right-label">CTRL function</span>' // look in /styles/atom-slds.less
		};
	}

	onDidInsertSuggestion(options) {
		atom.notifications.addSuccess(options.suggestion.displayText + ' was inserted.');
	}
}
export default new BasicFunctionsProvider();
