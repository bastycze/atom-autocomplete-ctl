from glob import glob
import BeautifulSoup
import re
import sys
from HTMLParser import HTMLParser
reload(sys)
sys.setdefaultencoding('utf-8')

class wincc_function:
    def __init__(self, name, arguments, descr):
        self._name = name
        self._arguments = arguments
        self._url = "https://test-wincchelp.web.cern.ch/test-wincchelp/Control/{}.htm".format(name)
        self._descr = descr

    def __str__(self):
        snippet = self._name + "(" + ", ".join([ "${" + "{}:{}".format(i+1, arg.strip().split(" ")[-1]) + "}" for i, arg in enumerate(self._arguments) if arg ]) + ")${}".format(len(self._arguments) + 1)
        return  "\t{\n" +  "\t\t\"displayText\": \"{}\",\n\t\t\"descriptionMoreURL\": \"{}\",\n\t\t\"snippet\": \"{}\",\n\t\t\"description\": \"{}\"\n".format(self._name, self._url, snippet, self._descr) + "\t}"

parsed_functions = []
for helpfile in glob(sys.argv[1]):
    with open(helpfile, "r") as f:
        soup = BeautifulSoup.BeautifulSoup("\n".join(f.readlines()))
        d = soup.find('p', {'class': 'Sourcefett'})
        out = re.sub(r'<.*?>', '', HTMLParser().unescape(str(d)))

        for c in ["\n", "\t", "\r", "[", "]"]: out = out.replace(c, "")
        out = out.replace("\"",  r'\"')
        name = out[out.find(" "):out.find("(")].strip()
        arguments = out[out.find("(")+1:out.find(")")].split(",")
        arguments = filter(None, arguments)
        if not (name.find(" ")>=0 or name == ""):
            parsed_functions += [wincc_function(name, arguments, out)]

with open("out.json", "w") as f:
    f.write("[\n" + ",\n".join([str(p) for p in parsed_functions]) + "\n]")
